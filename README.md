# Atalian

A new Flutter project.

## Getting Started

How to run the app:

- RUN: flutter pub get
- change API URL in: lib/app/model/app_const.dart
- RUN app with command:
  - dev: flutter run --flavor dev
  - prod: flutter run --flavor prod
