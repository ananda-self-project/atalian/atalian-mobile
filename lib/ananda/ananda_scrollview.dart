import 'package:flutter/material.dart';

class AnandaScrollView extends StatelessWidget {
  final Widget child;
  final EdgeInsetsGeometry padding;
  final Color color;

  const AnandaScrollView({
    Key key,
    @required this.child,
    this.color = Colors.white,
    this.padding = const EdgeInsets.all(0),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color,
      child: CustomScrollView(
        slivers: [
          SliverFillRemaining(
            hasScrollBody: false,
            child: Container(
              padding: padding,
              child: child,
            ),
          ),
        ],
      ),
    );
  }
}
