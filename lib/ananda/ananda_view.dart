import 'package:flutter/material.dart';

import './index.dart';

class AnandaBadge extends StatelessWidget {
  final String number;
  final Color color;
  final Color bgcolor;

  const AnandaBadge({
    Key key,
    @required this.number,
    this.color = AnandaColor.primary,
    this.bgcolor = AnandaColor.secondary,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 16,
      // width: 16,
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: bgcolor,
        borderRadius: BorderRadius.circular(4),
      ),
      child: Text(
        number,
        style: TextStyle(color: Colors.white, fontSize: 13, fontWeight: FontWeight.bold),
      ),
    );
  }
}

class AnandaBody extends StatelessWidget {
  final Color pageColor;
  final Color bodyColor;
  final List<Widget> body;

  const AnandaBody({
    Key key,
    this.pageColor = Colors.white,
    this.bodyColor = Colors.white,
    @required this.body,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: pageColor,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Container(
              padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
              decoration: BoxDecoration(
                color: bodyColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25),
                  topRight: Radius.circular(25),
                ),
              ),
              child: AnandaScrollView(
                color: bodyColor,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: body,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class AnandaCard extends StatelessWidget {
  final EdgeInsetsGeometry padding;
  final List<Widget> children;
  final Color color;
  final double radius;

  const AnandaCard({
    Key key,
    @required this.children,
    this.padding = const EdgeInsets.fromLTRB(16, 16, 16, 0),
    this.color = Colors.white,
    this.radius = 23,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding,
      width: double.infinity,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(radius),
        // border: Border.all(
        //   color: color,
        //   // width:
        // ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: children,
      ),
    );
  }
}

class AnandaBottomSheet extends StatefulWidget {
  final List<Widget> children;
  final bool reverse;
  final double initialChildSize;
  final double minChildSize;
  final double maxChildSize;
  const AnandaBottomSheet({
    Key key,
    @required this.children,
    this.reverse = false,
    this.initialChildSize = 0.38,
    this.minChildSize = 0.2,
    this.maxChildSize = 0.38,
  }) : super(key: key);

  @override
  _AnandaBottomSheetState createState() => _AnandaBottomSheetState();
}

class _AnandaBottomSheetState extends State<AnandaBottomSheet> {
  bool reverse = false;

  @override
  void initState() {
    super.initState();
    setState(() {
      reverse = widget.reverse;
    });
  }

  _onStartScroll(ScrollMetrics metrics) {
    setState(() {
      reverse = false;
    });
  }

  _onUpdateScroll(ScrollMetrics metrics) {
    setState(() {
      reverse = false;
    });
  }

  _onEndScroll(ScrollMetrics metrics) {
    setState(() {
      reverse = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox.expand(
      child: DraggableScrollableSheet(
        initialChildSize: widget.initialChildSize,
        minChildSize: widget.minChildSize,
        maxChildSize: widget.maxChildSize,
        builder: (context, controller) {
          return Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25),
                topRight: Radius.circular(25),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.black12,
                  offset: Offset(0.0, 1),
                  blurRadius: 3,
                  spreadRadius: 5,
                ),
              ],
            ),
            padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
            child: NotificationListener<ScrollNotification>(
              onNotification: (scrollNotification) {
                if (scrollNotification is ScrollStartNotification) {
                  _onStartScroll(scrollNotification.metrics);
                } else if (scrollNotification is ScrollUpdateNotification) {
                  _onUpdateScroll(scrollNotification.metrics);
                } else if (scrollNotification is ScrollEndNotification) {
                  _onEndScroll(scrollNotification.metrics);
                }
                return true;
              },
              child: SingleChildScrollView(
                physics: NeverScrollableScrollPhysics(),
                reverse: reverse,
                child: Column(
                  children: [
                    // Center(
                    //   child: Image.asset(
                    //     "assets/images/icons/rectangle.png",
                    //     width: 74,
                    //     height: 7,
                    //   ),
                    // ),
                    // SizedBox(height: 16),
                    SingleChildScrollView(
                      reverse: reverse,
                      // physics: NeverScrollableScrollPhysics(),
                      controller: controller,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: widget.children,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}

// CLASS DATA
class FavoriteData {
  final String name;
  final String number;
  final bool isAll;

  FavoriteData({@required this.name, this.number = "", this.isAll = false});
}

class NamedIcon extends StatelessWidget {
  final IconData iconData;
  final String text;
  final VoidCallback onTap;
  final int notificationCount;

  const NamedIcon({
    Key key,
    this.onTap,
    @required this.text,
    @required this.iconData,
    this.notificationCount,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: 72,
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: Stack(
          alignment: Alignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 8,
                ),
                Icon(
                  iconData,
                  color: AnandaColor.primary,
                ),
                Text(text, overflow: TextOverflow.ellipsis),
              ],
            ),
            Positioned(
              top: 0,
              right: 0,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 6, vertical: 2),
                decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.red),
                alignment: Alignment.center,
                child: Text(
                  '$notificationCount',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
