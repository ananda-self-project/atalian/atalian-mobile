import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import './index.dart';

class AnandaInput extends StatelessWidget {
  final String title;

  final FocusNode focusNode;
  final int maxLength;
  final bool obscureText;
  final String label;
  final String hint;
  final String errorText;
  final TextInputType keyboardType;
  final Icon icon;
  final IconButton suffixIcon;
  final TextEditingController controller;
  final Function(String) onChanged;
  final Function(String) validator;
  final Function(String) onFieldSubmitted;
  final String ini;
  final int maxLines;
  final double rad;
  final Color cursorColor;

  const AnandaInput({
    Key key,
    this.title,
    @required this.controller,
    this.focusNode,
    this.maxLength = 50,
    this.obscureText = false,
    @required this.hint,
    this.label,
    this.errorText,
    this.keyboardType = TextInputType.text,
    this.icon,
    this.suffixIcon,
    this.onChanged,
    this.validator,
    this.onFieldSubmitted,
    this.ini,
    this.rad = 10,
    this.maxLines = 1,
    this.cursorColor = AnandaColor.secondary,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        title != null
            ? Text(
                title,
                style: black13,
              )
            : SizedBox(),
        SizedBox(
          height: 8,
        ),
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(16),
          ),
          child: TextFormField(
            style: black18,
            maxLines: maxLines,
            initialValue: ini,
            obscureText: obscureText,
            maxLength: maxLength,
            cursorColor: cursorColor,
            keyboardType: keyboardType,
            keyboardAppearance: Brightness.light,
            textAlign: TextAlign.left,
            decoration: anandaInputDecoration(
              label: label,
              hint: hint,
              errorText: errorText,
              icon: icon,
              suffixIcon: suffixIcon,
              rad: rad,
            ),
            focusNode: focusNode,
            controller: controller,
            onChanged: onChanged,
            validator: validator,
            onFieldSubmitted: onFieldSubmitted,
          ),
        ),
      ],
    );
  }
}

InputDecoration anandaInputDecoration({
  String hint,
  String label,
  String errorText,
  Icon icon,
  IconButton suffixIcon,
  double rad,
}) {
  return InputDecoration(
    counterText: '',
    hintText: hint,
    labelText: label,
    errorText: errorText, // != null ? errorText : null,
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(rad),
      borderSide: const BorderSide(color: Colors.white),
    ),
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(rad),
      borderSide: const BorderSide(color: Colors.white),
    ),
    errorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(rad),
      borderSide: const BorderSide(color: Colors.red),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(rad),
      borderSide: const BorderSide(color: Colors.red),
    ),
    prefixIcon: icon,
    suffixIcon: suffixIcon,
    fillColor: AnandaColor.secondary.withOpacity(0.3),
    filled: true,
  );
}

class AnandaDropdown extends StatelessWidget {
  final String type;
  final List<String> listType;
  final Function(String) onChanged;

  const AnandaDropdown({
    Key key,
    @required this.type,
    @required this.listType,
    @required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          height: 60,
          width: double.infinity,
          padding: const EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            color: AnandaColor.secondary.withOpacity(0.3),
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: Colors.white),
          ),
          child: DropdownButtonHideUnderline(
            child: DropdownButton<String>(
              value: type,
              icon: Icon(
                Icons.keyboard_arrow_down,
                color: Colors.black,
              ),
              iconSize: 24,
              elevation: 16,
              style: TextStyle(color: Colors.black),
              onChanged: (String newValue) {
                onChanged(newValue);
              },
              items: listType.map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(
                    value,
                    style: black18,
                  ),
                );
              }).toList(),
            ),
          ),
        ),
      ],
    );
  }
}

class AnandaLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: CircularProgressIndicator(
        backgroundColor: Colors.white,
      ),
      height: 17.0,
      width: 17.0,
    );
  }
}
