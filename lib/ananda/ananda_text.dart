import 'package:flutter/material.dart';

import './index.dart';

// Bekerja
const TextStyle anandaTitle = TextStyle(
  fontWeight: FontWeight.w800,
  fontSize: 40.0,
  color: AnandaColor.secondary,
  fontFamily: 'Dosis',
);

const TextStyle white18 = TextStyle(
  color: Colors.white,
  fontSize: 18.0,
);

const TextStyle secondary18 = TextStyle(
  color: AnandaColor.secondary,
  fontSize: 18.0,
);

const TextStyle white18Bold = TextStyle(
  color: Colors.white,
  fontSize: 18.0,
  fontWeight: FontWeight.w700,
);

const TextStyle secondary18Bold = TextStyle(
  color: AnandaColor.secondary,
  fontSize: 18.0,
  fontWeight: FontWeight.w700,
);

const TextStyle white16 = TextStyle(
  color: Colors.white,
  fontSize: 16.0,
);

const TextStyle secondary16 = TextStyle(
  color: AnandaColor.secondary,
  fontSize: 16.0,
);

const TextStyle white16Bold = TextStyle(
  color: Colors.white,
  fontSize: 16.0,
  fontWeight: FontWeight.w700,
);

const TextStyle secondary16Bold = TextStyle(
  color: AnandaColor.secondary,
  fontSize: 16.0,
  fontWeight: FontWeight.w700,
);

// Size: 13
const TextStyle white13 = TextStyle(
  color: Colors.white,
  fontSize: 13.0,
);

const TextStyle black13 = TextStyle(
  color: Colors.black,
  fontSize: 13.0,
);

const TextStyle black18 = TextStyle(
  color: Colors.black,
  fontSize: 18.0,
);

const TextStyle secondary13 = TextStyle(
  color: AnandaColor.secondary,
  fontSize: 13.0,
);

const TextStyle white13Bold = TextStyle(
  color: Colors.white,
  fontSize: 13.0,
  fontWeight: FontWeight.w700,
);

const TextStyle secondary13Bold = TextStyle(
  color: AnandaColor.secondary,
  fontSize: 13.0,
  fontWeight: FontWeight.w700,
);
