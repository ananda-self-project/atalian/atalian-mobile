import 'package:flutter/material.dart';
import './index.dart';

class AnandaButton extends StatelessWidget {
  final Color backgroundColor;
  final String title;
  final Function onPressed;
  final Color splashColor;
  final double borderRadius;
  final MainAxisAlignment alignment;
  final bool isLoading;
  final EdgeInsetsGeometry margin;
  final bool boldText;

  AnandaButton({
    this.backgroundColor = AnandaColor.primary,
    @required this.title,
    @required this.onPressed,
    this.splashColor = Colors.white,
    this.borderRadius = 10.0,
    this.alignment = MainAxisAlignment.center,
    this.isLoading = false,
    this.margin = const EdgeInsets.only(top: 16.0),
    this.boldText = true,
  });

  RoundedRectangleBorder _myShape() {
    return RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(borderRadius),
    );
  }

  Widget _myChild() {
    return Row(
      mainAxisAlignment: alignment,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
          child: isLoading
              ? AnandaLoading()
              : Text(
                  title.toUpperCase(),
                  style: boldText ? white16Bold : secondary16Bold,
                ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      // padding: const EdgeInsets.only(left: 20.0, right: 20.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: FlatButton(
              shape: _myShape(),
              splashColor: splashColor,
              color: this.backgroundColor,
              child: _myChild(),
              onPressed: onPressed,
            ),
          ),
        ],
      ),
    );
  }
}

class AnandaButtonNew extends StatelessWidget {
  final String title;
  final bool active;
  final Function onPressed;
  final double height;
  final double width;
  final double borderRadius;
  final bool boldText;

  const AnandaButtonNew({
    Key key,
    @required this.title,
    this.active = false,
    this.onPressed,
    this.height = 50,
    this.width,
    this.borderRadius = 10,
    this.boldText = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var w = width == null ? MediaQuery.of(context).size.width : width;

    return AnandaInk(
      onPressed: onPressed,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        height: height,
        width: (w / 2),
        decoration: BoxDecoration(
          color: active ? AnandaColor.secondary : AnandaColor.secondary.withOpacity(0.17),
          borderRadius: BorderRadius.circular(borderRadius),
          border: Border.all(color: AnandaColor.secondary //active ? AnandaColor.secondary : AnandaColor.containerType,
              // width:
              ),
        ),
        child: Center(
          child: Text(
            title.toUpperCase(),
            style: active ? white16Bold : secondary16Bold,
          ),
        ),
      ),
    );
  }
}

class AnandaButtonIcon extends StatelessWidget {
  final String title;
  final bool active;
  final Function onPressed;

  const AnandaButtonIcon({Key key, @required this.title, this.active = false, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnandaInk(
      onPressed: onPressed,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        height: 70,
        // width: (w / 2) - (16 * 3),
        decoration: BoxDecoration(
          color: active ? AnandaColor.secondary : AnandaColor.secondary.withOpacity(0.17),
          borderRadius: BorderRadius.circular(35),
          border: Border.all(color: AnandaColor.secondary //active ? AnandaColor.secondary : AnandaColor.containerType,
              // width:
              ),
        ),
        child: Center(
          child: Row(
            children: [
              Text(
                title,
                style: white18,
              ),
              SizedBox(width: 16),
              Container(
                height: 40,
                width: 40,
                decoration: BoxDecoration(
                  color: active ? Colors.white.withOpacity(0.17) : AnandaColor.secondary.withOpacity(0.17),
                  borderRadius: BorderRadius.circular(23),
                ),
                child: Center(
                  child: Image.asset(
                    active ? "assets/images/icons/shield.png" : "assets/images/icons/arrow-right.png",
                    width: active ? 20 : 16,
                    height: active ? 20 : 14,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class AnandaInk extends StatelessWidget {
  final Function onPressed;
  final Color color;
  final Color highlightColor;
  final Widget child;
  final double radius;

  const AnandaInk({
    Key key,
    this.onPressed,
    this.color = Colors.white,
    @required this.child,
    this.highlightColor = Colors.white,
    this.radius = 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: InkWell(
        onTap: onPressed,
        splashColor: color,
        child: child,
        highlightColor: highlightColor,
        customBorder: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(radius),
        ),
      ),
    );
  }
}
