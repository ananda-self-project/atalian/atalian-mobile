import 'package:flutter/material.dart';

class AnandaAppBar extends PreferredSize {
  final String title;
  final bool centerTitle;
  final List<Widget> actions;
  final Widget leading;
  final Color backgroundColor;
  final bool result;
  // final String imageIcon;

  AnandaAppBar({
    @required this.title,
    this.centerTitle = true,
    this.actions = const [],
    this.leading,
    this.backgroundColor = Colors.white,
    this.result = false,
    // this.imageIcon = "assets/images/icons/phone.png",
  });

  @override
  Size get preferredSize => Size.fromHeight(60);

  @override
  Widget build(BuildContext context) {
    final w = MediaQuery.of(context).size.width;

    return AppBar(
      leading: leading != null
          ? leading
          : IconButton(
              icon: Icon(Icons.arrow_back),
              // new Image.asset(
              //   "assets/images/icons/back.png",
              //   height: 16,
              //   width: 16,
              // ),
              onPressed: () => Navigator.of(context).pop(),
            ),
      title: Container(
        // color: Colors.blue,
        width: w - 150,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // new Image.asset(
            //   imageIcon,
            //   height: 20,
            //   width: 18,
            // ),
            // SizedBox(
            //   width: 4,
            // ),
            Text(
              title,
              // style: ananda20Black,
            )
          ],
        ),
      ),
      centerTitle: centerTitle,
      backgroundColor: backgroundColor,
      elevation: 0.0,
      bottomOpacity: 0.0,
      brightness: Brightness.light,
      actions: actions,
    );
  }
}
