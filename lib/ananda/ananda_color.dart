import 'package:flutter/material.dart';

class AnandaColor {
  static const primary = Color(0xFFb2481b);
  static const secondary = Color(0xFFeab586);
}
