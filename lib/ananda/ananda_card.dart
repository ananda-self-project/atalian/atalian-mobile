import 'package:flutter/material.dart';
import 'dart:math';

import './index.dart';

class AnandaCardTwo extends StatelessWidget {
  final Widget child;
  final double height;
  final double radius;
  final EdgeInsetsGeometry margin;
  final EdgeInsetsGeometry padding;
  final Function onPressed;
  final Color gColor;
  final Color borderColor;
  final Color sColor;

  AnandaCardTwo({
    this.child,
    this.height = 125.0,
    this.radius = 10.0,
    this.margin = const EdgeInsets.only(left: 16.0, top: 16.0, right: 16.0),
    this.padding = const EdgeInsets.all(20.0),
    this.onPressed,
    this.gColor = AnandaColor.secondary,
    this.borderColor,
    this.sColor = Colors.white,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: margin,
      child: Container(
        // height: height,
        width: double.infinity,
        decoration: BoxDecoration(
          color: gColor,
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 10.0,
              offset: Offset(0, 1.0),
            )
          ],
          borderRadius: BorderRadius.all(
            Radius.circular(
              radius,
            ),
          ),
          border: borderColor == null ? null : Border.all(color: borderColor),
        ),
        child: Material(
          type: MaterialType.transparency,
          child: InkWell(
            customBorder: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(radius),
            ),
            splashColor: sColor,
            onTap: onPressed,
            child: Container(
              padding: padding,
              child: child,
            ),
          ),
        ),
      ),
    );
  }
}

class AnandaActivityCard extends StatelessWidget {
  final int index;
  final String image;
  final String title;
  final String description;
  final Function onPressed;
  final List<Color> gColor;
  final bool reverse;
  final double radius;
  final String note;
  final String status;

  AnandaActivityCard({
    this.index,
    this.image,
    this.title,
    this.description,
    this.onPressed,
    this.gColor,
    this.reverse = false,
    this.radius = 10,
    this.note = "",
    this.status = "PE",
  });

  int ranNumber() {
    Random random = new Random();
    int randomNumber = random.nextInt(4);
    return randomNumber;
  }

  @override
  Widget build(BuildContext context) {
    return AnandaCardTwo(
      radius: radius,
      gColor: Colors.white,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  title,
                  // style: reverse ? ananda14White : ananda20White,
                ),
                SizedBox(height: 10.0),
                Text(
                  description,
                  // style: !reverse ? ananda12White : ananda20White,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  softWrap: true,
                ),
                SizedBox(height: note == null || note.isEmpty || note == "-" ? 0 : 20.0),
                note == null || note.isEmpty || note == "-"
                    ? SizedBox()
                    : Text(
                        note,
                        // style: ananda14White,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        softWrap: true,
                      ),
              ],
            ),
          ),
        ],
      ),
      onPressed: onPressed,
    );
  }
}
