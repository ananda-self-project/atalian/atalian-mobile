import 'dart:convert';
import 'dart:async';
import 'dart:io';

import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../app_const.dart';
import '../http_exception.dart';

const header = {
  HttpHeaders.contentTypeHeader: "application/json",
  // HttpHeaders.acceptCharsetHeader: "utf-8",
};

class AuthModel with ChangeNotifier {
  String email;
  String name;
  int type;
  int id;
  String token;

  void setAuth(dynamic data) {
    id = data["id"];
    email = data["email"];
    name = data["name"];
    type = data["type"];
    id = data["id"];
    token = data["token"];
    notifyListeners();
  }

  Future<bool> tryAuth() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extractedUserData = json.decode(prefs.getString('userData'));
    setAuth(extractedUserData);
    return false;
  }

  bool get isAuth {
    return token != null;
  }

  int get userType {
    return type;
  }

  Future<void> login(String email, String password) async {
    var data = json.encode(
      {
        'email': email,
        'password': password,
      },
    );
    try {
      final response = await http.post(
        APP.URL_AUTH,
        headers: header,
        body: data,
      );

      if (response.statusCode != 200) {
        throw HttpException(response);
      }
      final responseData = json.decode(response.body);

      setUserData(responseData);
    } catch (e) {
      throw e;
    }
  }

  void setUserData(dynamic data) async {
    setAuth(data["data"]);

    final prefs = await SharedPreferences.getInstance();

    final userData = json.encode(
      {
        "id": id,
        "name": name,
        "email": email,
        "type": type,
        "token": token,
      },
    );
    prefs.setString("userData", userData);
  }

  Future<void> logout() async {
    email = null;
    name = null;
    type = null;
    token = null;
    notifyListeners();
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('userData');
  }
}
