import 'dart:convert';
import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../app_const.dart';
import '../http_exception.dart';

const header = {
  HttpHeaders.contentTypeHeader: "application/json",
  HttpHeaders.acceptHeader: "application/json",
  // HttpHeaders.acceptCharsetHeader: "utf-8",
};

headerToken(String token) => {
      HttpHeaders.contentTypeHeader: "application/json",
      HttpHeaders.acceptHeader: "application/json",
      HttpHeaders.authorizationHeader: "Bearer $token",
      // HttpHeaders.acceptCharsetHeader: "utf-8",
    };

class LaporanData {
  final int id;
  final String judul;
  final String deskripsi;
  final String photo;
  final String namaTeknisi;
  final String namaClient;
  final int status;
  final int rating;
  final String createdAt;

  LaporanData({
    @required this.id,
    @required this.judul,
    @required this.deskripsi,
    @required this.photo,
    @required this.namaClient,
    @required this.namaTeknisi,
    @required this.rating,
    @required this.status,
    @required this.createdAt,
  });

  factory LaporanData.fromJson(Map<String, dynamic> data) {
    return LaporanData(
      id: data["id"],
      judul: data["judul"],
      deskripsi: data["deskripsi"],
      photo: data["photo"],
      namaClient: data["client_name"],
      namaTeknisi: data["teknisi_name"],
      rating: data["rating"],
      status: data["status"],
      createdAt: data["created_at"],
    );
  }
}

class UserData {
  final int id;
  final String name;
  UserData({
    @required this.id,
    @required this.name,
  });

  factory UserData.fromJson(Map<String, dynamic> data) {
    return UserData(
      id: data["id"],
      name: data["name"],
    );
  }
}

class LaporanResponse {
  List<LaporanData> data;

  LaporanResponse({this.data});

  factory LaporanResponse.fromJson(List<dynamic> data) {
    List<LaporanData> bgList = data.map((e) => LaporanData.fromJson(e)).toList();

    return LaporanResponse(data: bgList);
  }
}

class UserResponse {
  List<UserData> data;

  UserResponse({this.data});

  factory UserResponse.fromJson(List<dynamic> data) {
    List<UserData> bgList = data.map((e) => UserData.fromJson(e)).toList();

    return UserResponse(data: bgList);
  }
}

class LaporanModel with ChangeNotifier {
  List<LaporanData> _data = [];

  List<LaporanData> get dataLaporan {
    return [..._data];
  }

  Future<void> getData({String token}) async {
    try {
      final response = await http.get(
        "${APP.URL_LAPORAN}",
        headers: headerToken(token),
      );

      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);

        var data = LaporanResponse.fromJson(responseData["data"]);

        this._data = data.data;

        notifyListeners();
      } else {
        throw HttpException(response);
      }
    } catch (e) {
      throw (e);
    }
  }

  Future<void> apiGet({
    @required String token,
    @required String url,
  }) async {
    print(url);
    try {
      final response = await http.get(
        url,
        headers: headerToken(token),
      );
      throw HttpException(response);
    } catch (e) {
      throw (e);
    }
  }
}
