import 'package:http/http.dart' as http;

class HttpException implements Exception {
  final http.Response response;

  HttpException(this.response);

  // @override
  // String toString() {
  //   return message;
  //   // return super.toString(); // Instance of HttpException
  // }
}
