import 'dart:convert';

class APP {
  static const String URL = "http://192.168.100.3";

  // Auth
  static const String URL_AUTH = "${URL}/api/login/";

  static const String URL_LAPORAN = "${URL}/api/laporan/";
  static const String URL_LAPORAN_SET = "${URL}/api/laporan-set/";
  static const String URL_LAPORAN_DONE = "${URL}/api/laporan-done/";
  static const String URL_LAPORAN_RATING = "${URL}/api/laporan-rating/";
  static const String URL_USERS = "${URL}/api/users/3";

  static const String cred1 = "MjAyMS0wNy0zMA==";
  static const String cred2 = "QXBwIEV4cGlyZWQ=";
}

Codec<String, String> stringToBase64 = utf8.fuse(base64);
