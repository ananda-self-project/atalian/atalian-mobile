import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// Widget
import '../../../ananda/index.dart';

// Model
import '../../model/auth/auth_model.dart';
import '../../model/http_exception.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey();

  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  final _passwordFocus = FocusNode();

  bool _isLoading = false;
  bool _passwordVisible = true;

  String _emailError;
  String _passwordError;

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _passwordFocus.dispose();
  }

  void _login() async {
    setState(() {
      _emailError = null;
      _passwordError = null;
    });

    if (!_formKey.currentState.validate() || _isLoading) {
      return;
    }

    _formKey.currentState.save();

    _setLoading(true);

    try {
      await Provider.of<AuthModel>(context, listen: false).login(
        _emailController.text,
        _passwordController.text,
      );
      _setLoading(false);
    } on HttpException catch (e) {
      _setLoading(false);

      return _showMyDialog("Email atau password salah.");
    }
  }

  Future<void> _showMyDialog(String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Alert'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(message),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Close'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _setLoading(bool isLoad) {
    setState(() {
      _isLoading = isLoad;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnandaBody(
        body: [
          SizedBox(
            height: 50.0,
          ),
          Image.asset(
            'images/logo.png',
            width: 200,
          ),
          SizedBox(
            height: 50.0,
          ),
          Form(
            key: _formKey,
            child: Column(
              children: [
                AnandaInput(
                  controller: _emailController,
                  hint: "Email",
                  errorText: _emailError,
                  icon: Icon(
                    Icons.mail,
                    color: AnandaColor.primary,
                    size: 23,
                  ),
                  suffixIcon: _emailController.text.length > 0
                      ? IconButton(
                          icon: Icon(
                            Icons.clear,
                            color: Colors.black,
                            size: 17.0,
                          ),
                          splashColor: AnandaColor.secondary,
                          onPressed: () => setState(() {
                            Future.microtask(() => _emailController.clear());
                          }),
                        )
                      : null,
                  onChanged: (val) => setState(() {}),
                  validator: (val) {
                    if (val.isEmpty) {
                      return 'Email tidak boleh kosong.';
                    }

                    Pattern pattern =
                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                    RegExp regex = new RegExp(pattern);
                    if (!regex.hasMatch(val)) return 'Email tidak valid.';

                    return null;
                  },
                  onFieldSubmitted: (_) => _passwordFocus.requestFocus(),
                ),
                SizedBox(height: 8),
                AnandaInput(
                  controller: _passwordController,
                  focusNode: _passwordFocus,
                  hint: "Kata Sandi",
                  errorText: _passwordError,
                  obscureText: _passwordVisible,
                  icon: Icon(
                    Icons.lock,
                    color: AnandaColor.primary,
                    size: 23,
                  ),
                  suffixIcon: IconButton(
                    icon: Icon(
                      _passwordVisible ? Icons.visibility : Icons.visibility_off,
                      color: AnandaColor.primary,
                    ),
                    onPressed: () => setState(() {
                      _passwordVisible = !_passwordVisible;
                    }),
                  ),
                  onChanged: (val) => setState(() {}),
                  onFieldSubmitted: (_) {
                    _passwordFocus.unfocus();
                    _login();
                  },
                  validator: (val) {
                    if (val.isEmpty) {
                      return 'Kata Sandi tidak boleh kosong.';
                    }

                    return null;
                  },
                ),
                AnandaButton(
                  title: "Masuk",
                  isLoading: _isLoading,
                  boldText: true,
                  margin: const EdgeInsets.only(
                    top: 16,
                    bottom: 20,
                  ),
                  onPressed: () => _login(),
                ),
                SizedBox(
                  height: 16,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
