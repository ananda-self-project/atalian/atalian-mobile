import 'dart:math';
import 'dart:io';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

// Widget
import '../../../../ananda/index.dart';

// Model
import '../../../model/app_const.dart';

class LaporanCreate extends StatefulWidget {
  final String token;

  const LaporanCreate({
    Key key,
    @required this.token,
  }) : super(key: key);

  @override
  _LaporanCreateState createState() => _LaporanCreateState();
}

class _LaporanCreateState extends State<LaporanCreate> {
  final GlobalKey<FormState> _formKey = GlobalKey();

  final _title = TextEditingController();
  final _description = TextEditingController();

  String _titleError;
  String _descriptionError;

  bool _isLoading = false;
  bool _isInit = true;

  File _newImage;

  void _setLoading(bool isLoad) {
    setState(() {
      _isLoading = isLoad;
    });
  }

  Future<void> _openCamera() async {
    try {
      final picker = ImagePicker();

      final pickedFile = await picker.getImage(
        source: ImageSource.camera,
        imageQuality: 25,
      );

      int bytes = await File(pickedFile.path).length();
      var size = bytes / 1024 / 1024;
      if (size > 2.4) {
        return _showMyDialog("Foto tidak boleh lebih dari 2mb");
      }

      setState(() {
        _newImage = File(pickedFile.path);
      });
    } catch (e) {
      print(e);
    }
  }

  Future<void> _openGallery() async {
    if (await Permission.photos.request().isGranted) {
      try {
        final picker = ImagePicker();

        final pickedFile = await picker.getImage(
          source: ImageSource.gallery,
          imageQuality: 25,
        );

        int bytes = await File(pickedFile.path).length();
        var size = bytes / 1024 / 1024;
        if (size > 2.4) {
          return _showMyDialog("Foto tidak boleh lebih dari 2mb");
        }

        setState(() {
          _newImage = File(pickedFile.path);
        });
      } catch (e) {
        print(e);
      }
    } else {
      Permission.photos.request();
    }
  }

  Future<void> _showSelectionDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              // title: Text(""),
              content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                GestureDetector(
                  child: Text("Gallery"),
                  onTap: () {
                    _openGallery();
                    Navigator.of(context).pop();
                  },
                ),
                SizedBox(height: 20),
                GestureDetector(
                  child: Text("Camera"),
                  onTap: () {
                    _openCamera();
                    Navigator.of(context).pop();
                  },
                )
              ],
            ),
          ));
        });
  }

  Future<void> _showMyDialog(String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Alert'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(message),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Tutup'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _submit() async {
    if (_newImage == null) {
      return _showMyDialog("Foto belum dipilih");
    }

    if (!_formKey.currentState.validate() || _isLoading) {
      return;
    }

    _formKey.currentState.save();

    _setLoading(true);

    try {
      var r = http.MultipartRequest("POST", Uri.parse(APP.URL_LAPORAN));

      r.headers['Authorization'] = "Bearer ${widget.token}";

      r.fields['judul'] = _title.text;
      r.fields['deskripsi'] = _description.text;

      if (_newImage != null) {
        r.files.add(
          await http.MultipartFile.fromPath("photo", _newImage.path),
        );
      }

      var streamedResponse = await r.send();
      var response = await http.Response.fromStream(streamedResponse);

      final responseData = json.decode(response.body);

      _setLoading(false);

      if (responseData["success"] == true) {
        var data = {'msg': responseData["message"]};
        return Navigator.pop(context, data);
      }

      return _showMyDialog(responseData["message"]);
    } catch (e) {
      return _showMyDialog(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    final h = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AnandaColor.primary,
        title: Text(
          "Tambah Laporan",
        ),
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                AnandaInput(
                  controller: _title,
                  hint: "Judul",
                  errorText: _titleError,
                  onChanged: (val) => setState(() {}),
                  validator: (val) {
                    if (val.isEmpty) {
                      return 'Judul tidak boleh kosong.';
                    }

                    return null;
                  },
                ),
                AnandaInput(
                  controller: _description,
                  hint: "Deskripsi",
                  errorText: _descriptionError,
                  maxLines: 4,
                  maxLength: 220,
                  onChanged: (val) => setState(() {}),
                  validator: (val) {
                    if (val.isEmpty) {
                      return 'Deskripsi tidak boleh kosong.';
                    }

                    return null;
                  },
                ),
                SizedBox(
                  height: 16,
                ),
                _newImage == null
                    ? Container()
                    : Image.file(
                        _newImage,
                        width: 300,
                        height: 270,
                        key: ValueKey(new Random().nextInt(100)),
                        fit: BoxFit.cover,
                      ),
                AnandaButton(
                  title: "Tambah Foto",
                  boldText: true,
                  margin: const EdgeInsets.only(
                    top: 16,
                  ),
                  onPressed: () => _showSelectionDialog(context),
                ),
                AnandaButton(
                  title: "Tambah Laporan",
                  isLoading: _isLoading,
                  boldText: true,
                  margin: const EdgeInsets.only(
                    top: 16,
                  ),
                  onPressed: () => _submit(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
