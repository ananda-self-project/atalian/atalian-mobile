import 'dart:math';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

// Widget
import '../../../../ananda/index.dart';

// Model
import '../../../model/app_const.dart';
import '../../../model/auth/auth_model.dart';
import '../../../model/http_exception.dart';
import '../../../model/home/home_model.dart';

import './add_laporan.dart';
import './detail_laporan.dart';

class HomeUser extends StatefulWidget {
  const HomeUser({Key key}) : super(key: key);

  @override
  _HomeUserState createState() => _HomeUserState();
}

class _HomeUserState extends State<HomeUser> {
  bool _isLoading = false;
  bool _isInit = true;

  String _token;
  int _type;

  void _setLoading(bool loading) {
    setState(() {
      _isLoading = loading;
    });
  }

  @override
  void didChangeDependencies() async {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      var auth = Provider.of<AuthModel>(context);
      setState(() {
        _isLoading = false;
        _token = auth.token;
        _type = auth.type;
      });
    }
    _isInit = false;

    _getData();

    super.didChangeDependencies();
  }

  void _getData() async {
    if (_isLoading) {
      return;
    }

    _setLoading(true);

    try {
      await Provider.of<LaporanModel>(context, listen: false).getData(
        token: _token,
      );
      _setLoading(false);
    } on HttpException catch (e) {
      _setLoading(false);
      String errorMessage = "Error - ${e.response.statusCode}";
      return _showMyDialog(errorMessage);
    } catch (e) {
      _setLoading(false);
      const errorMessage = 'Terjadi Kesalahan.\nSilahkan coba beberapa saat lagi.';
      return _showMyDialog(errorMessage);
    }
  }

  Future<void> _showMyDialog(String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Alert'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(message),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Tutup'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _goTo(Widget w) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => w,
      ),
    ).then(
      (value) {
        if (value != null) {
          _showMyDialog(value['msg']);
        }
        _getData();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final h = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AnandaColor.primary,
        title: Text(
          "Atalian",
        ),
        centerTitle: true,
        actions: _type != 2
            ? []
            : [
                IconButton(
                  icon: FaIcon(
                    FontAwesomeIcons.plus,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    _goTo(
                      LaporanCreate(
                        token: _token,
                      ),
                    );
                  },
                ),
              ],
      ),
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Container(),
          Consumer<LaporanModel>(
            builder: (ctx, data, _) => _isLoading
                ? Column(
                    children: [
                      SizedBox(
                        height: h / 2 - 100,
                      ),
                      AnandaLoading(),
                    ],
                  )
                : data.dataLaporan.length <= 0
                    ? Column(
                        children: [
                          SizedBox(
                            height: h / 2 - 100,
                          ),
                          Text("Tidak ada data"),
                        ],
                      )
                    : Expanded(
                        child: SingleChildScrollView(
                          child: ListView.builder(
                            padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: data.dataLaporan.length,
                            itemBuilder: (ctx, index) {
                              var d = data.dataLaporan[index];
                              return cardData(
                                data: d,
                              );
                            },
                          ),
                        ),
                      ),
          ),
        ],
      ),
    );
  }

  Widget cardData({LaporanData data}) {
    return Column(
      children: [
        AnandaInk(
          onPressed: () {
            _goTo(
              LaporanDetail(
                data: data,
              ),
            );
          },
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(4)),
              // border: Border.all(
              //   color: Colors.grey[400],
              //   width: 1,
              // ),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey[200],
                  offset: Offset(0.0, 0.5),
                  blurRadius: 3,
                  spreadRadius: 2,
                )
              ],
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CircleAvatar(
                    radius: 35,
                    backgroundImage: NetworkImage(
                      data.photo == null ? APP.URL + "" : APP.URL + data.photo,
                    ),
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Expanded(
                    flex: 1,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          data.judul,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          softWrap: true,
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Text(
                          // owner,
                          data.deskripsi,
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: 14,
                          ),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          softWrap: true,
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Row(
                          children: [
                            data.status == 3 && data.rating != null
                                ? RatingBar.builder(
                                    ignoreGestures: true,
                                    initialRating: data.rating.toDouble(),
                                    minRating: 1,
                                    direction: Axis.horizontal,
                                    allowHalfRating: false,
                                    itemCount: 5,
                                    itemPadding: EdgeInsets.symmetric(horizontal: 1.0),
                                    itemSize: 20,
                                    itemBuilder: (context, _) => Icon(
                                      Icons.star,
                                      color: Colors.amber,
                                    ),
                                    onRatingUpdate: (rating) {
                                      print(rating);
                                    },
                                  )
                                : Container(),
                            Spacer(),
                            AnandaBadge(
                              number: data.status == 1
                                  ? "Belum Diproses"
                                  : data.status == 2
                                      ? "Sedang Diproses"
                                      : "Selesai",
                              bgcolor: data.status == 1
                                  ? Colors.orange
                                  : data.status == 2
                                      ? Colors.blue
                                      : Colors.green,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                ],
              ),
            ),
          ),
        ),
        SizedBox(
          height: 16,
        ),
      ],
    );
  }
}
