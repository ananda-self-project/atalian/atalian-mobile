import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

// Widget
import '../../../../ananda/index.dart';
import '../image_dialog.dart';

// Model
import '../../../model/app_const.dart';
import '../../../model/http_exception.dart';
import '../../../model/auth/auth_model.dart';
import '../../../model/home/home_model.dart';

class LaporanDetail extends StatefulWidget {
  final LaporanData data;

  const LaporanDetail({
    Key key,
    @required this.data,
  }) : super(key: key);

  @override
  _LaporanDetailState createState() => _LaporanDetailState();
}

class _LaporanDetailState extends State<LaporanDetail> {
  LaporanData data;
  List<UserData> _listTeknisi = [];
  List<String> _listTeknisiName = [];
  String _teknisi = "Pilih Teknisi";

  String _token;
  int _type;

  bool _isLoading = false;
  bool _isInit = true;

  int _rating = 3;

  void _setLoading(bool loading) {
    setState(() {
      _isLoading = loading;
    });
  }

  @override
  void didChangeDependencies() async {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      var auth = Provider.of<AuthModel>(context);
      setState(() {
        _isLoading = false;
        _token = auth.token;
        _type = auth.type;
      });
      if (_type == 1) {
        _getData();
      }
    }
    _isInit = false;

    super.didChangeDependencies();
  }

  @override
  void initState() {
    setState(() {
      data = widget.data;
    });
    super.initState();
  }

  void _getData() async {
    if (_isLoading) {
      return;
    }

    _setLoading(true);

    try {
      await Provider.of<LaporanModel>(context, listen: false).apiGet(
        token: _token,
        url: APP.URL_USERS,
      );
      _setLoading(false);
    } on HttpException catch (e) {
      _setLoading(false);

      final responseData = json.decode(e.response.body);

      var data = UserResponse.fromJson(responseData["data"]);

      List<UserData> teknisi = data.data;

      List<String> teknisiName = ["Pilih Teknisi"];
      teknisi.forEach((element) {
        teknisiName.add(element.name);
      });

      setState(() {
        _listTeknisi = teknisi;
        _listTeknisiName = teknisiName;
      });

      if (e.response.statusCode != 200) {
        String errorMessage = "Error - ${e.response.statusCode}";
        return _showMyDialog(errorMessage);
      }
    } catch (e) {
      _setLoading(false);
      const errorMessage = 'Terjadi Kesalahan.\nSilahkan coba beberapa saat lagi.';
      return _showMyDialog(errorMessage);
    }
  }

  Future<void> _showMyDialog(String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Alert'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(message),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Tutup'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  String _publishAt() {
    DateTime s = DateTime.parse(data.createdAt).add(Duration(hours: 7));

    return DateFormat('dd MMMM y HH:mm').format(s);
  }

  void _setTeknisi() async {
    if (_teknisi == "Pilih Teknisi") {
      _showMyDialog("Pilih teknisi terlebih dahulu!");
    }

    var idTeknisi = _listTeknisi.firstWhere((element) => element.name == _teknisi);

    _updateLaporan(url: "${APP.URL_LAPORAN_SET}${data.id}/${idTeknisi.id}");
  }

  void _setTeknisiDone() {
    _updateLaporan(url: "${APP.URL_LAPORAN_DONE}${data.id}");
  }

  void _setRating() {
    _updateLaporan(url: "${APP.URL_LAPORAN_RATING}${data.id}/${_rating}");
  }

  void _updateLaporan({String url}) async {
    if (_isLoading) {
      return;
    }

    _setLoading(true);

    try {
      await Provider.of<LaporanModel>(context, listen: false).apiGet(
        token: _token,
        url: url,
      );
      _setLoading(false);
    } on HttpException catch (e) {
      _setLoading(false);

      final responseData = json.decode(e.response.body);

      var dt = LaporanData.fromJson(responseData["data"]);

      setState(() {
        data = dt;
      });

      if (e.response.statusCode != 200) {
        String errorMessage = "Error - ${e.response.statusCode}";
        return _showMyDialog(errorMessage);
      }

      return _showMyDialog(responseData["message"]);
    } catch (e) {
      _setLoading(false);
      const errorMessage = 'Terjadi Kesalahan.\nSilahkan coba beberapa saat lagi.';
      return _showMyDialog(errorMessage);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AnandaColor.primary,
        title: Text(
          "Atalian",
        ),
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    height: 250,
                    child: Stack(
                      children: [
                        AnandaInk(
                          onPressed: () async {
                            await showDialog(
                              context: context,
                              builder: (_) => ImageDialog(
                                image: Image.network(
                                  data.photo == null ? APP.URL + "/storage/not-found.png" : APP.URL + data.photo,
                                ),
                              ),
                            );
                          },
                          child: Image.network(
                            data.photo == null ? APP.URL + "/storage/not-found.png" : APP.URL + data.photo,
                            fit: BoxFit.cover,
                            width: 1000.0,
                          ),
                        ),
                        Positioned(
                          child: AnandaBadge(
                            number: data.status == 1
                                ? "Belum Diproses"
                                : data.status == 2
                                    ? "Sedang Diproses"
                                    : "Selesai",
                            bgcolor: data.status == 1
                                ? Colors.orange
                                : data.status == 2
                                    ? Colors.blue
                                    : Colors.green,
                          ),
                          right: 16,
                          bottom: 16,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          data.judul,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Text(
                          _publishAt(),
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: 12,
                          ),
                        ),
                        Divider(),
                        SizedBox(
                          height: 8,
                        ),
                        Row(
                          children: [
                            Text(
                              "Pelapor: ",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 12,
                              ),
                            ),
                            Text(
                              data.namaClient,
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Row(
                          children: [
                            Text(
                              "Teknisi: ",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 12,
                              ),
                            ),
                            Text(
                              data.namaTeknisi,
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        data.status == 3 && data.rating != null
                            ? Row(
                                children: [
                                  Text(
                                    "Rating: ",
                                    style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 12,
                                    ),
                                  ),
                                  RatingBar.builder(
                                    ignoreGestures: true,
                                    initialRating: data.rating.toDouble(),
                                    minRating: 1,
                                    direction: Axis.horizontal,
                                    allowHalfRating: false,
                                    itemCount: 5,
                                    itemPadding: EdgeInsets.symmetric(horizontal: 1.0),
                                    itemSize: 20,
                                    itemBuilder: (context, _) => Icon(
                                      Icons.star,
                                      color: Colors.amber,
                                    ),
                                    onRatingUpdate: (rating) {
                                      print(rating);
                                    },
                                  ),
                                ],
                              )
                            : Container(),
                        Divider(),
                        SizedBox(
                          height: 8,
                        ),
                        Text(
                          data.deskripsi,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          _type == 1 && data.status == 1 ? supervisor() : Container(),
          _type == 3 && data.status == 2 ? teknisi() : Container(),
          _type == 2 && data.status == 3 && data.rating == null ? client() : Container(),
        ],
      ),
    );
  }

  Widget supervisor() {
    return bottomAction(children: [
      AnandaDropdown(
        type: _teknisi,
        listType: _listTeknisiName,
        onChanged: (v) {
          if (v != "Pilih Teknisi") {
            setState(() {
              _teknisi = v;
            });
          }
        },
      ),
      AnandaButton(
        title: "Simpan",
        isLoading: _isLoading,
        boldText: true,
        margin: const EdgeInsets.only(
          top: 16,
        ),
        onPressed: () => _setTeknisi(),
      ),
    ]);
  }

  Widget teknisi() {
    return bottomAction(
      children: [
        AnandaButton(
          title: "Selesai",
          isLoading: _isLoading,
          boldText: true,
          margin: const EdgeInsets.only(
            top: 16,
          ),
          onPressed: () => _setTeknisiDone(),
        ),
      ],
    );
  }

  Widget client() {
    return bottomAction(
      children: [
        Center(
          child: RatingBar.builder(
            initialRating: 3,
            minRating: 1,
            direction: Axis.horizontal,
            allowHalfRating: false,
            itemCount: 5,
            itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
            itemBuilder: (context, _) => Icon(
              Icons.star,
              color: Colors.amber,
            ),
            onRatingUpdate: (rating) {
              setState(() {
                _rating = rating.toInt();
              });
            },
          ),
        ),
        AnandaButton(
          title: "Simpan",
          isLoading: _isLoading,
          boldText: true,
          margin: const EdgeInsets.only(
            top: 16,
          ),
          onPressed: () => _setRating(),
        ),
      ],
    );
  }

  Widget bottomAction({List<Widget> children}) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: children,
        ),
      ),
    );
  }
}
