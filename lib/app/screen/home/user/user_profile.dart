import 'dart:math';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

// Widget
import '../../../../ananda/index.dart';

// Model
import '../../../model/app_const.dart';
import '../../../model/auth/auth_model.dart';
import '../../../model/http_exception.dart';
import '../../../model/home/home_model.dart';

import '../image_dialog.dart';

class UserProfile extends StatefulWidget {
  const UserProfile({Key key}) : super(key: key);

  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  String _token;
  int _id;
  String _email;
  String _name;
  String _image;

  bool _isLoading = false;
  bool _isInit = true;

  void _setLoading(bool loading) {
    setState(() {
      _isLoading = loading;
    });
  }

  @override
  void didChangeDependencies() async {
    if (_isInit) {
      _setData();
    }
    _isInit = false;

    super.didChangeDependencies();
  }

  void _setData() {
    setState(() {
      _isLoading = true;
    });
    var auth = Provider.of<AuthModel>(context, listen: false);
    setState(() {
      _isLoading = false;
      _token = auth.token;
      _id = auth.id;
      _name = auth.name;
      _email = auth.email;
    });
  }

  Future<void> _showMyDialog(String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Alert'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(message),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Tutup'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _goTo(Widget widget) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => widget,
      ),
    ).then(
      (value) {
        if (value != null) {
          _showMyDialog(value['msg']);
          _setData();
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: AnandaColor.primary,
        title: Text("Profile"),
      ),
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsets.all(16),
        child: Column(
          children: [
            Row(
              children: [
                CircleAvatar(
                  backgroundColor: Colors.transparent,
                  child: GestureDetector(
                    onTap: () async {
                      await showDialog(
                        context: context,
                        builder: (_) => ImageDialog(
                          image: Image.asset('images/logo.png'),
                        ),
                      );
                    },
                  ),
                  radius: 50,
                  backgroundImage: AssetImage('images/logo.png'),
                ),
                SizedBox(
                  width: 16,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      _name,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      _email,
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 14,
                      ),
                    )
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 32,
            ),
            Divider(
              thickness: 1,
            ),
            menuButton(
              icon: FontAwesomeIcons.signOutAlt,
              title: "Logout",
              onPressed: () {
                Provider.of<AuthModel>(context, listen: false).logout();
              },
            ),
            Divider(
              thickness: 1,
            ),
          ],
        ),
      ),
    );
  }

  Widget menuButton({IconData icon, String title, Function onPressed}) {
    return Column(
      children: [
        SizedBox(
          height: 8,
        ),
        AnandaInk(
          onPressed: onPressed,
          child: Container(
            // decoration: BoxDecoration(
            //   borderRadius: BorderRadius.all(Radius.circular(4)),
            //   border: Border.all(
            //     color: AnandaColor.secondary,
            //     width: 2,
            //   ),
            // ),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    child: Center(
                      child: FaIcon(
                        icon,
                        size: 20,
                        // color: AnandaColor.primary,
                      ),
                    ),
                    width: 50,
                  ),
                  SizedBox(
                    width: 32,
                  ),
                  Text(
                    title,
                    style: TextStyle(
                      // color: AnandaColor.primary,
                      fontWeight: FontWeight.bold,
                      fontSize: 14,
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
