import 'package:flutter/material.dart';

class ImageDialog extends StatelessWidget {
  final Image image;

  const ImageDialog({
    Key key,
    @required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final h = MediaQuery.of(context).size.height;
    final w = MediaQuery.of(context).size.width;
    return Dialog(
      child: Container(
        width: w - 100,
        height: h / 2,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: image.image,
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
