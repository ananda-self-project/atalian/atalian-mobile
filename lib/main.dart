import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

// Widget
import './ananda/index.dart';

// Screen
import './app/screen/auth/login_screen.dart';
import './app/screen/home/home_screen.dart';

// Model
import './app/model/auth/auth_model.dart';
import './app/model/home/home_model.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: AuthModel(),
        ),
        ChangeNotifierProvider.value(
          value: LaporanModel(),
        ),
      ],
      child: Consumer<AuthModel>(
        builder: (ctx, auth, _) => GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: MaterialApp(
            title: 'Wembley Cafe',
            theme: ThemeData(
              primaryColor: AnandaColor.primary,
              accentColor: AnandaColor.secondary,
              visualDensity: VisualDensity.adaptivePlatformDensity,
              fontFamily: 'NeoSans',
              // appBarTheme: AppBarTheme(brightness: Brightness.light),
            ),
            home: auth.isAuth
                ? HomeScreen()
                : FutureBuilder(
                    future: auth.tryAuth(),
                    builder: (ctx, authResultSnapshot) => authResultSnapshot.connectionState == ConnectionState.waiting ? SplashScreen() : LoginScreen(),
                  ),
            debugShowCheckedModeBanner: false,
          ),
        ),
      ),
    );
  }
}

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;

    return Scaffold(
      body: Container(
        width: w,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            new CupertinoActivityIndicator(),
          ],
        ),
      ),
    );
  }
}
